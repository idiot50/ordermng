package com.orderMng.repo;

import com.orderMng.model.authenz.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsersRepository extends JpaRepository<Users, Long> {

    List<Users> findByUserNameIgnoreCase(String firstName);
}
