package com.orderMng.repo;

import java.util.List;

import com.orderMng.model.Customer;
import org.springframework.data.repository.CrudRepository;


public interface CustomerRepository extends CrudRepository<Customer, Long>{
	List<Customer> findByFullName(String lastName);
}
