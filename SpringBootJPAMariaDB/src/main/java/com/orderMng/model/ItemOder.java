package com.orderMng.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ItemOder")
public class ItemOder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "createOder")
    private Date createOder;

    @Column(name = "status")
    private Long status;

    @Column(name = "shipService")
    private String shipService;

    @Column(name = "orderId")
    private String orderId;

    @Column(name = "customerId")
    private Long customerId;

    @Column(name = "itemId")
    private Long itemId;


}
