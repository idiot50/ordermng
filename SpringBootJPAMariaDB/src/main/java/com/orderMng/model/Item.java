package com.orderMng.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Item")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "itemName")
    private String itemName;

    @Column(name = "webSale")
    private String webSale;

    @Column(name = "size")
    private Double size;

    @Column(name = "amount")
    private Long amount;

    @Column(name = "priceWeb")
    private Double priceWeb;

    @Column(name = "priceVnd")
    private Double priceVnd;

    @Column(name = "isFreeShip")
    private Long isFreeShip;

    @Column(name = "description")
    private String description;

}
