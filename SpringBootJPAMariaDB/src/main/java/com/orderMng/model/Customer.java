package com.orderMng.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "customer")
public class Customer implements Serializable {

	private static final long serialVersionUID = -3009157732242241606L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "fullName")
	private String fullName;

	@Column(name = "telephone")
	private String telephone;

	@Column(name = "link")
	private String link;

	@Column(name = "email")
	private String email;

	@Column(name = "address")
	private String address;

	@Column(name = "description")
	private String description;


	public Customer(String fullName, String description) {
		this.fullName = fullName;
		this.description = description;
	}

	@Override
	public String toString() {
		return String.format("Customer[id=%d, fullName='%s', link='%s', telephone='%s', email='%s', address='%s', description='%s']", id,fullName, link,telephone, email, address,description);
	}
}