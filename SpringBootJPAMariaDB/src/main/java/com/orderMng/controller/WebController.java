package com.orderMng.controller;

import java.util.Arrays;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.orderMng.model.Customer;
import com.orderMng.repo.CustomerRepository;

@Api(value = "WebController", description = "REST Apis related to Student Entity!!!!")
@RestController
public class WebController {
	
	@Autowired
	CustomerRepository repository;
	
	@GetMapping("/save")
	public String process(){
		
		repository.save(Arrays.asList(new Customer("Jack", "Smith"), 
										new Customer("Adam", "Johnson"),
										new Customer("Kim", "Smith"),
										new Customer("David", "Williams"),
										new Customer("Peter", "Davis")));
		
		return "Done";
	}

	@ApiOperation(value = "Get list of Students in the System ", response = String.class, tags = "findall")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success|OK"),
			@ApiResponse(code = 401, message = "not authorized!"),
			@ApiResponse(code = 403, message = "forbidden!!!"),
			@ApiResponse(code = 404, message = "not found!!!") })
	@GetMapping("/findall")
	public String findAll(){
		
		String result = "";
		
		for(Customer cust : repository.findAll()){
			result += cust + "</br>";
		}
		
		return result;
	}
	
	@GetMapping("/findbyid")
	public String findById(
			@RequestParam("id") long id){
		String result = "";
		result = repository.findOne(id).toString();
		return result;
	}
	
	@GetMapping("/findbylastname")
	public String fetchDataByLastName(@RequestParam("lastname") String lastName){
		String result = "";
		
		for(Customer cust: repository.findByFullName(lastName)){
			result += cust + "</br>"; 
		}
		
		return result;
	}
}