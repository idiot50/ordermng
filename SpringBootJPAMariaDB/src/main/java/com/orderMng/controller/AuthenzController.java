package com.orderMng.controller;


import com.orderMng.repo.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenzController {

    @Autowired
    UsersRepository usersRepository;

    public AuthenzController() {
    }

    @GetMapping("/findUser")
    public String findUser(
            @RequestParam("userName") String userName){
        String result = "";
        result = usersRepository.findByUserNameIgnoreCase(userName).toString();
        return result;
    }
}
